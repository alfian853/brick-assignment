import gzip
import json
import time
import pandas as pd
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from seleniumwire import webdriver

BACKUP_FILE_NAME = 'data-checkpoint.txt'


def parse_response(byte_data: bytes) -> json:
    try:
        return json.loads(gzip.decompress(byte_data))
    except:
        return json.loads(byte_data)


def safe_call(fun):
    try:
        return fun()
    except:
        return None


def save_data(obj):
    with open(BACKUP_FILE_NAME, 'w') as f:
        f.write(json.dumps(obj))


def load_data() -> json:
    with open(BACKUP_FILE_NAME, 'r') as f:
        return json.load(f)


# scrap item data from handphone category page
def scrap_item_list(driver: webdriver, number_of_item: int) -> list:
    url = 'https://www.tokopedia.com/p/handphone-tablet/handphone'
    print('Start scraping item data from page:', url)
    data = []
    page_nav_xpath = '/html/body/div[1]/div/div[2]/div/div[2]/div/div[2]/div[3]/div[2]/div[4]/div/button[{}]'

    driver.get(url)

    # scroll down to load the page navigation
    driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
    WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, page_nav_xpath.format(2))))

    last_index = 0
    for page_num in range(1, 5):
        print('Scraping page', page_num)
        driver.requests.clear()
        target_xpath = page_nav_xpath.format(page_num + 1)
        page_nav_el = driver.find_element_by_xpath(target_xpath)
        page_nav_el.click()

        page_nav_el = None
        # loop until the active button is changed to the page_num
        while page_nav_el is None:
            time.sleep(1)
            page_nav_el = safe_call(lambda: driver.find_element_by_xpath(target_xpath))
            if page_nav_el is not None:
                page_btn_active = safe_call(
                    lambda: page_nav_el.parent.find_element_by_class_name('css-omqoof-unf-pagination-item-active'))
                if page_btn_active is None or page_btn_active.text != str(page_num):
                    page_nav_el = None
            driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")

        # read data from the Graphql API response
        for i in range(last_index, len(driver.requests)):
            request = driver.requests[i]
            if request.response and request.url.startswith('https://gql.tokopedia.com/'):
                try:
                    json_body = parse_response(request.response.body)[0]
                    if 'CategoryProducts' in json_body['data'].keys():
                        print('from page', page_num, json_body['data']['CategoryProducts']['data'][0]['name'])
                        data += json_body['data']['CategoryProducts']['data']
                except Exception as e:
                    print('error occured')
            last_index = i

        if len(data) >= number_of_item:
            break

    print('Scraping complete!')
    return data[:number_of_item]


def scrap_description(driver: webdriver, url) -> str:
    xpath_description = '/html/body/div[1]/div/div[2]/div[2]/div[5]/div[2]/div[2]/div/span/span/div'

    driver.get(url)
    WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, xpath_description)))

    return driver.find_element_by_xpath(xpath_description).get_attribute('innerText')


if __name__ == '__main__':
    # Create a new instance of the Firefox driver
    driver = webdriver.Firefox()
    target_csv_name = 'tokopedia-top-100-handphone.csv'

    data = scrap_item_list(driver, 100)

    print('Backup scraping result to file:', BACKUP_FILE_NAME)
    save_data(data)

    # uncomment this if want load the saved data
    # data = load_data()

    # load data to DataFrame
    df = pd.DataFrame(pd.json_normalize(data, max_level=2),
                      columns=['name', 'url', 'imageUrl', 'price', 'rating', 'shop.name', 'description'],
                      dtype='object')

    df.rename(columns={'shop.name': 'store'}, inplace=True)

    # Product description scraping process
    try:
        print('Start to scraping product descriptions')
        for i, data in df.iterrows():
            print('Get description for row: {}, productName: {}'.format(i, data['name']))
            df._set_value(i, 'description', value=scrap_description(driver, data.url))
    except:
        print('An Error is occurred, break the process.')
    finally:
        print('Saving to file:', target_csv_name)
        df.to_csv(target_csv_name, index=False)
        driver.close()
